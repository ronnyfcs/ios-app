# README #

A simple IOS App that was created in order to learn Swift (other technologies) and how IOS Apps are created. 

### Objectives ###

* Learn the Swift language and XCode.

* Learn and incorporate SwiftUI, Core Data, and API calls. 

### What do I need? ###

* Designated for MacOS.

* You will need XCode and Swift 5 (or higher).

### Contributor ###
-------------------

Ronny Fuentes <Ronnyf@uoregon.edu>
